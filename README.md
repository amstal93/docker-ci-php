# CI PHP

[![pipeline status](https://gitlab.com/florenttorregrosa-docker/images/docker-ci-php/badges/develop/pipeline.svg)](https://gitlab.com/florenttorregrosa-docker/images/docker-ci-php/-/commits/develop)

This repo contains the build for the maintained docker container https://hub.docker.com/r/florenttorregrosa/ci-php.
